#!/bin/bash

set -o errexit
set -o nounset

cleanup() {
    rm -f testmachine{,.pub}
    docker-compose down -v --rmi all --remove-orphans
}

trap cleanup EXIT
ssh-keygen -b 2048 -t rsa -f testmachine -q -N ""
./generate-orchestration-files.rb "$1"
docker-compose build --pull
docker-compose up --force-recreate --abort-on-container-exit
