#!/usr/bin/ruby2.7
require 'mustache'

DISTRIBUTIONS = [ "debian:bullseye", "debian:bookworm", "ubuntu:focal", "ubuntu:jammy" ]

if ARGV.length < 1
  puts "Too few arguments"
  exit 1
end
ssh_base_port = ARGV[0].to_i
testmachines = []
DISTRIBUTIONS.each_with_index do |distribution,index|
  counter = index + 1
  ssh_port = ssh_base_port + counter
  testmachines.push({
    distribution: distribution,
    index: counter,
    ssh_port: ssh_port,
    port_mapping: "\"#{ssh_port}:#{ssh_port}\""
  })
end

containers = Mustache.render(
  File.read("templates/container.mustache"),
  testmachines: testmachines
).rstrip!
links = Mustache.render(
  File.read("templates/link.mustache"),
  testmachines: testmachines
).rstrip!
docker_compose = Mustache.render(
  File.read("templates/docker_compose.mustache"),
  {containers: containers, links: links}
)
File.write("docker-compose.yml", docker_compose)

hosts = Mustache.render(
  File.read("templates/hosts.mustache"),
  testmachines: testmachines
)
File.write("hosts", hosts)
